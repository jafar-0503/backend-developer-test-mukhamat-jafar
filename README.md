
## Environment:
- Java version: 17
- Maven version: 3.*
- Spring Boot version: 3.1.9
- PostgreSQL version: 12
- Docker version: 24.0.7
- Docker Compose version: v2.23.0

## Requirements:
1. Below is the endpoint for the `POST` API endpoint for `Register User & Generate Token`.

    
    `POST /auth/register-user`

Request Body:
```json
{
  "name": "lorem ipsum",
  "email":"lorem@mail.com",
  "roles":"ROLE_USER",
  "password":"lorem123"
}
```

Response Body:
```json
{
  "status": true,
  "data": {
    "id": 2,
    "createdBy": "SYS",
    "fromDate": "2024-01-28T02:56:10.001+00:00",
    "lastModifiedBy": "SYS",
    "toDate": "2024-01-28T02:56:10.001+00:00",
    "name": "lorem ipsum",
    "email": "lorem@mail.com",
    "password": "$2a$10$1nj20qKCT2I/eiuzmvBD...Z0bX7rGaR5BdhD0Mx7898MULL3BrDq",
    "roles": "ROLE_USER"
  },
  "message": "User Added Successfully"
}
```
`POST /auth/generate-token`

Request Body:
```json
{
  "username": "lorem ipsum",
  "password":"lorem123"
}
```

Response Body:
```json
{
  "access_token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJsb3JlbSBpcHN1bSIsImlhdCI6MTcwNjQxMTgxNSwiZXhwIjoxNzA2NDEzNjE1fQ.KVSJfTIN49a-ToPKNMRWQIcPTNYOZtiJg5oxhJ9_DUc"
}
```

2. Below is the endpoint for the `GET` API endpoint for `TASKS`.

`GET api/v1/tasks`
cURL:
```json
curl --location 'http://localhost:8082/api/v1/tasks' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJKaG9uIiwiaWF0IjoxNzA2NDEwMzYwLCJleHAiOjE3MDY0MTIxNjB9.8FC-Rybe10ZVXAD8SGqVE_vWFEGT4nMMENRkvQ1-s6M'
```

`GET api/v1/tasks/{id}`
```json
curl --location 'http://localhost:8082/api/v1/tasks/4' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJsb3JlbSBpcHN1bSIsImlhdCI6MTcwNjQxMzQxMywiZXhwIjoxNzA2NDE1MjEzfQ.5zrlOFgQWfVHhnUW0XGRCMO88WimZBX_oQlZMFTsZLg'
```

Response Body
```json
{
    "status": true,
    "data": {
        "id": 4,
        "createdBy": "SYS",
        "fromDate": "2024-01-28T00:14:19.020+00:00",
        "lastModifiedBy": "SYS",
        "toDate": "2024-01-28T00:14:19.020+00:00",
        "title": "Task 4",
        "description": "four Task",
        "dueDate": "2024-01-31T00:00:00.000+00:00",
        "status": "todo"
    },
    "message": "Tasks retrieved successfully"
}
```

`POST /api/v1/tasks`

```json
curl --location 'http://localhost:8082/api/v1/tasks' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJsb3JlbSBpcHN1bSIsImlhdCI6MTcwNjQxMzQxMywiZXhwIjoxNzA2NDE1MjEzfQ.5zrlOFgQWfVHhnUW0XGRCMO88WimZBX_oQlZMFTsZLg' \
--data '{
    "title":"Task 5",
    "description":"four Task",
    "dueDate":"2024-01-31",
    "status":"todo"
}'
```

Response Body
```json
{
  "status": true,
  "data": {
    "id": 5,
    "createdBy": "SYS",
    "fromDate": "2024-01-28T03:48:52.270+00:00",
    "lastModifiedBy": "SYS",
    "toDate": "2024-01-28T03:48:52.270+00:00",
    "title": "Task 5",
    "description": "four Task",
    "dueDate": "2024-01-31T00:00:00.000+00:00",
    "status": "todo"
  },
  "message": "Tasks added successfully"
}
```

`PUT /api/v1/tasks/{id}`
```json
curl --location --request PUT 'http://localhost:8082/api/v1/tasks/5' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJsb3JlbSBpcHN1bSIsImlhdCI6MTcwNjQxMzQxMywiZXhwIjoxNzA2NDE1MjEzfQ.5zrlOFgQWfVHhnUW0XGRCMO88WimZBX_oQlZMFTsZLg' \
--data '{
    "title":"Task 5",
    "description":"First Task",
    "dueDate":"2024-01-31",
    "status":"done"
}'
```

Response Body
```json
{
    "status": true,
    "data": {
        "id": 5,
        "createdBy": "SYS",
        "fromDate": "2024-01-28T03:48:52.270+00:00",
        "lastModifiedBy": "SYS",
        "toDate": "2024-01-28T03:51:09.954+00:00",
        "title": "Task 5",
        "description": "First Task",
        "dueDate": "2024-01-31T00:00:00.000+00:00",
        "status": "done"
    },
    "message": "Tasks edited successfully"
}
```

`DEL /api/v1/tasks/{id}`

## Commands
- Build Maven:
```bash
mvn clean install -DskipTests
```

- Run PostgreSQL in Docker:
```bash
docker run --name test -p 5432:5432 -e POSTGRES_DB=db-test -e POSTGRES_USER=test -e POSTGRES_PASSWORD=admin123 -d postgres:12
```
- Collection Postman:
```
https://drive.google.com/drive/folders/1x0DS5E36pjyreqmiodhz-6w1wxH434y0?usp=drive_link```