package backend.test.utils;

/**
 * @author jafar on 25/01/2024
 **/
public class Constant {
    public static final String SUCCESS_RETRIEVED = "Tasks retrieved successfully";
    public static final String TASKS_ADDED = "Tasks added successfully";
    public static final String TASKS_EDITED = "Tasks edited successfully";
    public static final String TASKS_DELETED = "Tasks deleted successfully";
    public static final String TASKS_ID = "Tasks Id ";
    public static final String NOT_EXIST = " is not exist";
    public static final String USER_ADDED = "User Added Successfully";
    public static final String USER_NOT_FOUND = "User not found ";
    public static final String INVALID_USER = "invalid user request !";
}