package backend.test.config.response;

import lombok.Data;

@Data
public class AccessToken {
    private Object access_token;

    public AccessToken(String access_token) {
        this.access_token = access_token;
    }
}