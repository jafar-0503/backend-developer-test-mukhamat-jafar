package backend.test.service;

import backend.test.config.error.ResourceNotFoundException;
import backend.test.config.response.BaseResponse;
import backend.test.model.Task;
import backend.test.repository.TaskRepository;
import backend.test.utils.Constant;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author jafar on 25/01/2024
 **/

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final ModelMapper modelMapper;

    public TaskService(TaskRepository taskRepository, ModelMapper modelMapper) {
        this.taskRepository = taskRepository;
        this.modelMapper = modelMapper;
    }

    public ResponseEntity<BaseResponse<Task>> getAllTask(){
        List<Task> tasks = taskRepository.findAll();

        Type targetType = new TypeToken<List<Task>>(){}.getType();
        List<Task> data= modelMapper.map(tasks, targetType);
        BaseResponse response = new BaseResponse(true, data, Constant.SUCCESS_RETRIEVED);

        return ResponseEntity.ok(response);
    }

    public ResponseEntity<BaseResponse<Task>> findById(Long id){
        Task task = taskRepository.findById(id).
            orElseThrow(()-> new ResourceNotFoundException(Constant.TASKS_ID + id + Constant.NOT_EXIST));
        Task data = modelMapper.map(task, Task.class);
        BaseResponse response = new BaseResponse(true, data, Constant.SUCCESS_RETRIEVED);

        return ResponseEntity.ok(response);
    }

    public ResponseEntity<BaseResponse<Task>> addTask(Task task){
        Task tasks = modelMapper.map(task, Task.class);
        Task saved = taskRepository.save(tasks);
        Task data = modelMapper.map(saved, Task.class);
        BaseResponse response = new BaseResponse(true, data, Constant.TASKS_ADDED);

        return ResponseEntity.ok(response);
    }

    public ResponseEntity<BaseResponse<Task>> editTasks(Task task, Long id){
        Task tasks = taskRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException(Constant.TASKS_ID + id + Constant.NOT_EXIST));
        tasks.setTitle(task.getTitle());
        tasks.setDescription(task.getDescription());
        tasks.setDueDate(task.getDueDate());
        tasks.setStatus(task.getStatus());
        taskRepository.save(tasks);

        Task data = modelMapper.map(tasks, Task.class);
        BaseResponse response = new BaseResponse(true, data, Constant.TASKS_EDITED);

        return ResponseEntity.ok(response);
    }

    //Remove-Employee
    public ResponseEntity<BaseResponse<Task>> deleteTaks(Long id){
        Task employee = taskRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException(Constant.TASKS_ID + id + Constant.NOT_EXIST));
        taskRepository.deleteById(id);
        Task data = modelMapper.map(employee, Task.class);
        BaseResponse response = new BaseResponse(true, data, Constant.TASKS_DELETED);

        return ResponseEntity.ok(response);
    }
}