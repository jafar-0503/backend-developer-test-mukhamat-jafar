package backend.test.service;

import backend.test.model.UserInfo;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author jafar on 28/01/2024
 **/
public class UserInfoDetails implements UserDetails {

   private final String name;
   private final String password;
   private final List<GrantedAuthority> authorities;

   public UserInfoDetails(UserInfo userInfo) {
      name = userInfo.getName();
      password = userInfo.getPassword();
      authorities = Arrays.stream(userInfo.getRoles().split(","))
          .map(SimpleGrantedAuthority::new)
          .collect(Collectors.toList());
   }

   @Override
   public Collection<? extends GrantedAuthority> getAuthorities() {
      return authorities;
   }

   @Override
   public String getPassword() {
      return password;
   }

   @Override
   public String getUsername() {
      return name;
   }

   @Override
   public boolean isAccountNonExpired() {
      return true;
   }

   @Override
   public boolean isAccountNonLocked() {
      return true;
   }

   @Override
   public boolean isCredentialsNonExpired() {
      return true;
   }

   @Override
   public boolean isEnabled() {
      return true;
   }
}
