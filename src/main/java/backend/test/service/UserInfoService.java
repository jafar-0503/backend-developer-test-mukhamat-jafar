package backend.test.service;

import backend.test.config.response.BaseResponse;
import backend.test.model.UserInfo;
import backend.test.repository.UserInfoRepository;
import backend.test.utils.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author jafar on 28/01/2024
 **/

@Service
public class UserInfoService implements UserDetailsService {

   @Autowired
   private UserInfoRepository userInfoRepository;
   @Autowired
   private PasswordEncoder passwordEncoder;


   @Override
   public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

      Optional<UserInfo> userDetail = userInfoRepository.findByName(username);

      // Converting userDetail to UserDetails
      return userDetail.map(UserInfoDetails::new)
          .orElseThrow(() -> new UsernameNotFoundException(Constant.USER_NOT_FOUND + username));
   }

   public ResponseEntity<BaseResponse<UserInfo>> addUser(UserInfo userInfo) {
      userInfo.setPassword(passwordEncoder.encode(userInfo.getPassword()));
      userInfoRepository.save(userInfo);

      BaseResponse response = new BaseResponse(true, userInfo, Constant.USER_ADDED);
      return ResponseEntity.ok(response);
   }
}
