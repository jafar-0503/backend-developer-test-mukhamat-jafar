package backend.test.controller;

import backend.test.config.response.BaseResponse;
import backend.test.model.Task;
import backend.test.service.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author jafar on 25/01/2024
 **/

@RestController
@RequestMapping("api/v1")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/tasks")
    public ResponseEntity<BaseResponse<Task>> getTasks(){
        return taskService.getAllTask();
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("tasks/{id}")
    public ResponseEntity<BaseResponse<Task>> getEmployeeById(@PathVariable Long id){
        return taskService.findById(id);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/tasks")
    public ResponseEntity<BaseResponse<Task>> addTasks(@RequestBody Task task){
        return taskService.addTask(task);
    }

    @PreAuthorize("isAuthenticated()")
    @PutMapping("tasks/{id}")
    public ResponseEntity<BaseResponse<Task>> editTasks(@RequestBody Task editTasks, @PathVariable Long id){
        return taskService.editTasks(editTasks, id);
    }

    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("tasks/{id}")
    public ResponseEntity<BaseResponse<Task>> deleteTask(@PathVariable Long id){
        return taskService.deleteTaks(id);
    }
}
