package backend.test.controller;

import backend.test.config.response.AccessToken;
import backend.test.config.response.BaseResponse;
import backend.test.model.AuthRequest;
import backend.test.model.UserInfo;
import backend.test.service.AuthService;
import backend.test.service.UserInfoService;
import backend.test.utils.Constant;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

/**
 * @author jafar on 28/01/2024
 **/
@RestController
@RequestMapping("/auth")
public class UserController {

   private final UserInfoService userInfoService;
   private final AuthService authService;
   private final AuthenticationManager authenticationManager;

   public UserController(UserInfoService userInfoService, AuthService authService, AuthenticationManager authenticationManager) {
      this.userInfoService = userInfoService;
      this.authService = authService;
      this.authenticationManager = authenticationManager;
   }

   @PostMapping("/register-user")
   public ResponseEntity<BaseResponse<UserInfo>> addNewUser(@RequestBody UserInfo userInfo) {

      return userInfoService.addUser(userInfo);
   }

   @PostMapping("/generate-token")
   public ResponseEntity<AccessToken> authenticateAndGetToken(@RequestBody AuthRequest authRequest) {
      Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
      if (authentication.isAuthenticated()) {
         return authService.generateToken(authRequest.getUsername());
      } else {
         throw new UsernameNotFoundException(Constant.INVALID_USER);
      }
   }
}
