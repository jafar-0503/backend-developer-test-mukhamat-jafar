package backend.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jafar on 28/01/2024
 **/

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthRequest extends BaseEntity{
   private String username;
   private String password;
}
