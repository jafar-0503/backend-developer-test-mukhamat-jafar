package backend.test.model;

import backend.test.auditable.Auditable;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author jafar on 25/01/2024
 **/

@Data
@NoArgsConstructor
@Entity
@Table(name = "task")
public class Task extends Auditable<String> {

    @Column(length = 100, unique = true, nullable = false, name ="title" )
    private String title;

    @Column(length = 250, nullable = false, name = "description")
    private String description;

    @Column(name = "due_date")
    private Date dueDate;

    @Column(length = 100, nullable = false, name = "status")
    private String status;
}