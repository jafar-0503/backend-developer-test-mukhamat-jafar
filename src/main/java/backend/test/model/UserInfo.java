package backend.test.model;

import backend.test.auditable.Auditable;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author jafar on 28/01/2024
 **/

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_info")
public class UserInfo extends Auditable<String> {

   private String name;
   private String email;
   private String password;
   private String roles;
}
