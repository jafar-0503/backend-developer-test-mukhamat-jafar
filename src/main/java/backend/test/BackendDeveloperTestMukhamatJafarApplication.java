package backend.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendDeveloperTestMukhamatJafarApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendDeveloperTestMukhamatJafarApplication.class, args);
	}

}
