package backend.test.repository;

import backend.test.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author jafar on 25/01/2024
 **/

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}
