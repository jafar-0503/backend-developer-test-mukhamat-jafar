package backend.test.repository;

import backend.test.model.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author jafar on 28/01/2024
 **/
@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Integer> {
   Optional<UserInfo> findByName(String username);
}
